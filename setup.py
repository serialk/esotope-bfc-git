#!/usr/bin/env python2
#-*- encoding: utf-8 -*-
"""
    esotope-bfc
    ~~~~~~~~~~~

    Esotope Brainfuck compiler (aka esotope-bfc) is an optimizing
    Brainfuck-to-C compiler. It aims at the best Brainfuck compiler ever,
    enabling many possible optimizations other compilers don't.
"""

from setuptools import setup, find_packages

setup(
    name = 'esotope-bfc',
    version = '0.1',
    url = 'https://code.google.com/p/esotope-bfc/',
    license = 'MIT',
    author = 'Kang Seonghoon',
    author_email = 'esotope+bfc@mearie.org',
    description = "the world's most optimizing Brainfuck-to-something compiler",
    long_description = __doc__,
    keywords = 'brainfuck',
    platforms = 'any',
    scripts = ['esotope-bfc'],
    packages = find_packages(),
)
